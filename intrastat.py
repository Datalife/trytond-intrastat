# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import (ModelSQL, ModelView, fields,
    DeactivableMixin)
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.pool import Pool
from trytond.exceptions import UserError
from trytond.i18n import gettext
from sql.functions import Function
import ssl
import re
import unicodedata
import urllib.request
from bs4 import BeautifulSoup

STATES = {
        'readonly': Eval('state').in_(['done', 'cancelled'])
}
context = ssl._create_unverified_context()

SOUP_CACHE = None


class Replace(Function):
    __slots__ = ()
    _function = 'REPLACE'


class IntrastatCode(DeactivableMixin, ModelSQL, ModelView):
    '''Intrastat Code'''
    __name__ = 'intrastat.code'

    code = fields.Char('Code', required=True, size=10)
    description = fields.Char('Description', translate=True)
    childs = fields.One2Many('intrastat.code', 'parent', 'Children')
    parent = fields.Many2One('intrastat.code', 'Parent', ondelete='RESTRICT',
        domain=['OR',
            ('parent', '=', None),
            ('parent.parent', '=', None),
            ('parent.parent.parent', '=', None),
        ])
    cn_subheading = fields.Function(fields.Boolean('CN Subheading'),
        'get_cn_subheading', searcher='search_cn_subheading')

    def get_rec_name(self, name):
        return "{} {}".format(self.code, self.description)

    @classmethod
    def search_rec_name(cls, name, clause):
        _, operator, value = clause
        Operator = fields.SQL_OPERATORS[operator]

        table = cls.__table__()
        query = table.select(table.id,
            where=(
                Operator(table.description, value)
                | Operator(table.code, value)
                | Operator(Replace(table.code, ' ', ''), value)
            )
        )
        return [('id', 'in', query)]

    @property
    def trimmed_code(self):
        return self.code.replace(' ', '')

    def get_cn_subheading(self, name=None):
        return self.childs is None

    @classmethod
    def search_cn_subheading(cls, name, clause):
        reverse_clause = {
            '=': '!=',
            '!=': '='
        }
        operator = clause[1] if clause[2] else reverse_clause[clause[1]]
        return [('childs', operator, None)]


class IntrastatTransportMode(DeactivableMixin, ModelSQL, ModelView):
    '''Intrastat Transport Mode'''
    __name__ = 'intrastat.transport_mode'

    code = fields.Char('Code', required=True)
    name = fields.Char('Name', translate=True)
    description = fields.Char('Description', translate=True)


class IntrastatTransactionType(DeactivableMixin, ModelSQL, ModelView):
    '''Intrastat Transaction Type'''
    __name__ = 'intrastat.transaction.type'

    code = fields.Char('Code', required=True)
    description = fields.Char('Description')

    def get_rec_name(self, name):
        return "{} {}".format(self.code, self.description)


class LoadCodesStart(ModelView):
    '''Start Load Intrastat Codes'''
    __name__ = 'intrastat.code.load_codes.start'

    chapter = fields.Selection('get_chapters', 'Chapter', sort=False,
        required=True)

    @classmethod
    def get_chapters(cls):
        p = re.compile('CAPÍTULO.*')
        chapters = set()

        if not SOUP_CACHE:
            return []
        chapters_p = list(SOUP_CACHE.find_all('p',
            id=lambda x: x and x.startswith('d1e')))
        for chapter_p in chapters_p:
            if p.match(chapter_p.getText()):
                value = unicodedata.normalize("NFKD",
                    chapter_p.getText())[-2:].strip(' ')
                label = chapter_p.findNext('p').text
                chapters.add((value, '{} - {}'.format(value, label)))

        return sorted(list(chapters), key=lambda x: int(x[0]))


class LoadCodesFinish(ModelView):
    '''Finish Load Intrastat Codes'''
    __name__ = 'intrastat.code.load_codes.finish'


class LoadCodes(Wizard):
    '''Intrastat Code Load Codes From URL'''
    __name__ = 'intrastat.code.load_codes'
    start_state = 'pre_load'

    pre_load = StateTransition()
    start = StateView('intrastat.code.load_codes.start',
        'intrastat.intrastat_load_codes_start_view', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Load', 'load', 'tryton-ok', default=True),
        ])
    load = StateTransition()
    finish = StateView('intrastat.code.load_codes.finish',
        'intrastat.intrastat_load_codes_finish_view', [
            Button('Load another chapter', 'start', 'tryton-ok', default=True),
            Button('OK', 'end', 'tryton-cancel'),
        ])

    def _format_text(self, description):
        return description.replace('–', '').replace('—', '').replace(
            '\n', '').strip(' ')

    def transition_pre_load(self):
        self._get_soap_cache()
        return 'start'

    def _get_soap_cache(self):
        global SOUP_CACHE
        pool = Pool()
        Conf = pool.get('intrastat.configuration')
        if not SOUP_CACHE:
            with urllib.request.urlopen(Conf(1).download_codes_url) as url:
                SOUP_CACHE = BeautifulSoup(url.read(), 'html.parser')
        return SOUP_CACHE

    def transition_load(self):
        pool = Pool()
        Code = pool.get('intrastat.code')

        _soup = self._get_soap_cache()
        chapter = _soup.find('p',
            id=lambda x: x and x.startswith('d1e'),
            text=lambda x: x and x.startswith('CAPÍTULO') and
                unicodedata.normalize("NFKD", x)[-2:].
                    strip(' ') == self.start.chapter)
        description_p = chapter.findNext('p')

        formatted_chapter = str(self.start.chapter).zfill(2)
        exist_root = Code.search([
            ('parent', '=', None),
            ('code', '=', formatted_chapter)
        ])
        if exist_root:
            raise UserError(gettext(
                'intrastat.msg_intrastat_code_load_codes_code_alread_exist',
                code=formatted_chapter))
        root = self._get_intrastat_code(description_p.text, formatted_chapter)
        root.save()
        table = description_p.findNext('table', attrs={'class': ['borderOj']})
        parent = root
        add_description = ''

        def _find_parent(code, parent):
            if not parent:
                return root
            if code.startswith(parent.code):
                return parent
            return _find_parent(code, parent.parent)

        for tr in table.findChildren('tr')[2:]:
            code = unicodedata.normalize("NFKD", tr.td.p.text).strip()
            values = list(tr.children)
            description = unicodedata.normalize(
                "NFKD", list(tr.children)[3].text) if len(values) > 3 else None
            if not description:
                continue
            if (code is None or not self._format_text(code)):
                add_description = description
                continue

            if (description.count('–') == add_description.count('–') + 1
                    and add_description):
                description = self._format_text(
                    add_description) + '. ' + self._format_text(
                    description)

            child = self._get_intrastat_code(
                self._format_text(description),
                code, parent=_find_parent(code, parent))
            child.save()
            parent = child
        return 'finish'

    def _get_intrastat_code(self, description, code, parent=None):
        pool = Pool()
        Code = pool.get('intrastat.code')
        return Code(description=description, code=code, parent=parent)
