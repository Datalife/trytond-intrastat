# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.model import ModelSingleton
from trytond.pyson import Id
from trytond.modules.company.model import (CompanyMultiValueMixin,
    CompanyValueMixin)
from trytond.pool import Pool

intrastat_names = ['declare_dispatch', 'declare_arrival', 'transport_mode',
    'transaction_type']


def default_func(field_name):
    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()
    return default


class Configuration(ModelSingleton, ModelSQL, ModelView,
        CompanyMultiValueMixin):
    '''Intrastat Configuration'''
    __name__ = 'intrastat.configuration'

    declare_dispatch = fields.MultiValue(fields.Boolean('Declare dispatch'))
    declare_arrival = fields.MultiValue(fields.Boolean('Declare arrival'))
    transport_mode = fields.MultiValue(fields.Many2One(
        'intrastat.transport_mode', 'Transport mode'))
    transaction_type = fields.MultiValue(fields.Many2One(
        'intrastat.transaction.type', 'Transaction type'))
    intrastat_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Intrastat Sequence',
        domain=[
            ('sequence_type', '=', Id('intrastat',
                'sequence_type_intrastat')),
        ]))
    download_codes_url = fields.Char('Download codes URL')

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in intrastat_names:
            return pool.get('intrastat.configuration.declaration.values')
        if field == 'intrastat_sequence':
            return pool.get('intrastat.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)

    default_intrastat_sequence = default_func('intrastat_sequence')
    default_declare_dispatch = default_func('declare_dispatch')
    default_declare_arrival = default_func('declare_arrival')


class ConfigurationSequence(ModelSQL, CompanyValueMixin):
    """ Intrastat Configuration Sequence"""
    __name__ = 'intrastat.configuration.sequence'

    intrastat_sequence = fields.Many2One(
        'ir.sequence', "Intrastat Sequence", required=True,
        domain=[
            ('sequence_type', '=', Id('intrastat',
                'sequence_type_intrastat')),
        ])

    @classmethod
    def default_intrastat_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id(
                'intrastat', 'sequence_intrastat')
        except KeyError:
            return None


class ConfigurationDeclarationValues(ModelSQL, CompanyValueMixin):
    """ Intrastat Configuration Sequence"""
    __name__ = 'intrastat.configuration.declaration.values'

    declare_dispatch = fields.Boolean('Declare dispatch')
    declare_arrival = fields.Boolean('Declare arrival')
    transport_mode = fields.Many2One(
        'intrastat.transport_mode', 'Transport mode')
    transaction_type = fields.Many2One(
        'intrastat.transaction.type', 'Transaction type')

    @staticmethod
    def default_declare_dispatch():
        return False

    @staticmethod
    def default_declare_arrival():
        return False
