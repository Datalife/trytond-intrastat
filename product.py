# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    intrastat_code = fields.Many2One('intrastat.code',
        'Intrastat code',
        domain=[('cn_subheading', '=', True)],
        states={'readonly': ~Eval('active')},)


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
