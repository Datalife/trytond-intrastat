==================
Intrastat Scenario
==================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.intrastat.tests.tools import configure_intrastat
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> import datetime
    >>> from decimal import Decimal
    >>> from dateutil.relativedelta import relativedelta
    >>> today = datetime.date.today()

Install intrastat::

    >>> config = activate_modules('intrastat')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']

    >>> Journal = Model.get('account.journal')
    >>> PaymentMethod = Model.get('account.invoice.payment.method')
    >>> cash_journal, = Journal.find([('type', '=', 'cash')])
    >>> cash_journal.save()
    >>> payment_method = PaymentMethod()
    >>> payment_method.name = 'Cash'
    >>> payment_method.journal = cash_journal
    >>> payment_method.credit_account = cash
    >>> payment_method.debit_account = cash
    >>> payment_method.save()

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create parties::

    >>> Subdivision = Model.get('country.subdivision')
    >>> SubdivisionType = Model.get('party.address.subdivision_type')
    >>> Country = Model.get('country.country')
    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> country_es = Country(name="Spain", code="ES", eu_member=True)
    >>> country_es.save()
    >>> sub_type, = SubdivisionType.find([('country_code', '=', country_es.code)])
    >>> sub_type.types = list(sub_type.types) + ['state']
    >>> sub_type.save()
    >>> madrid = Subdivision(
    ...     name="Madrid", code="ES-MA", type='state', country=country_es)
    >>> madrid.save()
    >>> address_es = customer.addresses.new()
    >>> address_es.name = 'Address 2'
    >>> address_es.country = country_es
    >>> address_es.subdivision = madrid
    >>> address_es.save()
    >>> customer.save()
    >>> company_addr = company.party.addresses[0]
    >>> company_addr.country = country_es
    >>> company_addr.subdivision = madrid
    >>> company_addr.save()

Create supplier::

    >>> country_de = Country(name="Germany", code="DE", eu_member=True)
    >>> country_de.save()
    >>> berlin = Subdivision(
    ...     name="Berlin", code="DE-BE", type='state', country=country_de)
    >>> berlin.save()
    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier', active=True)
    >>> supplier.save()
    >>> address = supplier.addresses[0]
    >>> address.country = country_de
    >>> address.subdivision = berlin
    >>> address.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> lost_found_loc, = Location.find([('type', '=', 'lost_found')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> internal_loc = Location(name='Internal', type='storage')
    >>> internal_loc.save()
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> warehouse_loc.address = address_es
    >>> warehouse_loc.save()

Configure Intrastat::

    >>> conf = configure_intrastat()
    >>> conf.declare_arrival = True
    >>> conf.save()
    >>> conf.intrastat_sequence.sequence_type.name
    'Intrastat'
    >>> conf.transport_mode.name
    'Sea'
    >>> conf.transaction_type.code
    'TT1'

Create Intrastat codes::

    >>> Code = Model.get('intrastat.code')
    >>> code_c1 = Code(code='04', description="DAIRY PRODUCE; BIRDS' EGGS; NATURAL HONEY; EDIBLE PRODUCTS OF ANIMAL ORIGIN,  NOT ELSEWHERE SPECIFIED OR INCLUDED")
    >>> code_c1.save()
    >>> code_g1 = Code(code='0401', parent=code_c1, description='Milk and cream')
    >>> code_g1.save()
    >>> code_p1 = Code(code='0401 10', description='Of a fat content, by weight, not exceeding 1 %', parent=code_g1)
    >>> code_p1.save()
    >>> code_c1 = Code(code='0401 10 10', description='In immediate packings of a net content not exceeding two litres', parent=code_p1)
    >>> code_c1.save()
    >>> code_c2 = Code(code='0401 10 90', description='Other', parent=code_p1)
    >>> code_c2.save()

Check Intrastat codes hierarchy::

    >>> len(code_g1.childs)
    1
    >>> code_p1.parent == code_g1
    True
    >>> len(code_p1.childs)
    2
    >>> code_c1.parent == code_c2.parent == code_p1
    True
    >>> code_c2.code
    '0401 10 90'
    >>> code_c2.rec_name
    '0401 10 90 Other'

Only 4 levels of hierarchy are allowed::

    >>> code_gc1 = Code(code='10', description='Other', parent=code_c1)
    >>> code_gc1.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Parent" in "Intrastat Code" is not valid according to its domain. - 

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')

    >>> unit, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.list_price = Decimal('30')
    >>> template.account_category = account_category
    >>> template.intrastat_code = code_g1
    >>> template.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Intrastat code" in "Product Template" is not valid according to its domain. - 
    >>> template.intrastat_code = code_c1
    >>> template.save()
    >>> product, = template.products

    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.type = 'service'
    >>> template.default_uom = unit
    >>> template.list_price = Decimal('30')
    >>> template.account_category = account_category
    >>> template.save()
    >>> service, = template.products

Create Intrastat Declaration::

    >>> Declaration = Model.get('intrastat.declaration')
    >>> declaration = Declaration(from_date=today,to_date=today, company=company, description='Declaration 1')
    >>> declaration.save()
    >>> declaration.number
    '1'
    >>> declaration.state
    'draft'

Create invoice and check declaration::

    >>> Invoice = Model.get('account.invoice')
    >>> invoice = Invoice(type='in')
    >>> invoice.party = supplier
    >>> invoice.invoice_date = today
    >>> invoice.accounting_date = today
    >>> line = invoice.lines.new()
    >>> line.product = product
    >>> line.quantity = 2
    >>> line.unit_price = Decimal('10')
    >>> invoice.save()
    >>> invoice.click('post')

    >>> declaration.click('create_lines')
    >>> line = declaration.lines[0]
    >>> invoice_line, = invoice.lines
    >>> line.invoice_line == invoice_line
    True
    >>> line.date == invoice.move.date
    True
    >>> line.intrastat_code.code
    '0401 10 10'
    >>> line.quantity
    2.0
    >>> line.amount
    Decimal('20.00')
    >>> line.product == invoice_line.product
    True
    >>> line.origin_country.name
    'Germany'
    >>> line.origin_subdivision.name
    'Berlin'
    >>> line.destination_country.name
    'Spain'
    >>> line.destination_subdivision.name
    'Madrid'
    >>> line.transport_mode.name
    'Sea'
    >>> line.transaction_.code
    'TT1'
    >>> line.net_weight
    2.0

Check summary:::

    >>> summary = declaration.summaries[0]
    >>> summary.declaration == declaration
    True
    >>> summary.intrastat_code.code
    '0401 10 10'
    >>> summary.type
    'arrival'
    >>> summary.origin_country.name
    'Germany'
    >>> summary.destination_country.name
    'Spain'
    >>> summary.amount == line.amount
    True
    >>> summary.destination_subdivision.name
    'Madrid'
    >>> line.transport_mode.name
    'Sea'
    >>> summary.transaction_.code
    'TT1'

Delete lines::

    >>> declaration.click('delete_lines')
    >>> len(declaration.lines)
    0

Create invoice with service::

    >>> Invoice = Model.get('account.invoice')
    >>> invoice2 = Invoice(type='in')
    >>> invoice2.party = supplier
    >>> invoice2.invoice_date = today
    >>> invoice2.accounting_date = today
    >>> line = invoice2.lines.new()
    >>> line.product = product
    >>> line.quantity = 2
    >>> line.unit_price = Decimal('10')
    >>> line = invoice2.lines.new()
    >>> line.product = product
    >>> line.quantity = 10
    >>> line.unit_price = Decimal('8')
    >>> line = invoice2.lines.new()
    >>> line.product = service
    >>> line.quantity = 1
    >>> line.unit_price = -Decimal('4')
    >>> invoice2.save()
    >>> invoice2.click('post')

Check distributed service lines::

    >>> declaration.click('create_lines')
    >>> len(declaration.lines)
    5
    >>> iline, = [l for l in invoice2.lines if l.product.type == 'service']
    >>> service_lines = [l for l in declaration.lines if l.invoice_line == iline]
    >>> sorted([l.amount for l in service_lines])
    [Decimal('-3.20'), Decimal('-0.80')]
    >>> not sum([l.quantity for l in service_lines])
    True
    >>> not sum([l.net_weight or 0 for l in service_lines])
    True

Do declaration::

    >>> declaration.click('do')
    >>> declaration.state
    'done'

Load codes::

    >>> code_c1 = Code(code='02')
    >>> code_c1.save()
    >>> load_codes = Wizard('intrastat.code.load_codes', [])
    >>> load_codes.form.chapter = '2'
    >>> load_codes.execute('load')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Intrastat code "02" already exists. - 
    >>> load_codes.form.chapter = '1'
    >>> load_codes.execute('load')
    >>> code, = Code.find([('parent', '=', None), ('code', '=', '01')])
    >>> code.description
    'ANIMALES VIVOS'
    >>> code2, = [code for code in code.childs if code.code == '0102']
    >>> code2.code
    '0102'
    >>> code2.description
    'Animales vivos de la especie bovina'
    >>> code21, = [code for code in code2.childs if code.code == '0102 21']
    >>> code21.code
    '0102 21'
    >>> code21.description
    'Bovinos domésticos. Reproductores de raza pura (13)'
    >>> code30, = [code for code in code21.childs if code.code == '0102 21 30']
    >>> code30.code
    '0102 21 30'
    >>> code30.description
    'Vacas'