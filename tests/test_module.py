# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class IntrastatTestCase(ModuleTestCase):
    """Test Intrastat module"""
    module = 'intrastat'
    extras = ['account_invoice_secondary_unit', 'sale', 'purchase',
        'product_measurements']


del ModuleTestCase
