# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from proteus import Model
import datetime

today = datetime.date.today()


def configure_intrastat():
    conf = Model.get('intrastat.configuration')(1)
    TransactionType = Model.get('intrastat.transaction.type')
    TransportMode = Model.get('intrastat.transport_mode')

    transport_mode, = TransportMode.find([('name', '=', 'Sea')])
    transaction_type = TransactionType(
        code='TT1',
        description='Transaction Type 1')
    transaction_type.save()
    conf.transaction_type = transaction_type
    conf.transport_mode = transport_mode
    conf.save()
    return conf
